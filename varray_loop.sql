# example of function, looping and varray

CREATE OR REPLACE PACKAGE bookExample
IS
TYPE bookTitleArray IS VARRAY (30) OF VARCHAR2(30);
FUNCTION getTitles(categoryInput books.category%TYPE) return bookTitleArray;
END bookExample;
/

CREATE OR REPLACE PACKAGE BODY bookExample
IS
FUNCTION getTitles(categoryInput books.category%TYPE) return bookTitleArray
AS
    titleArray  bookTitleArray;
BEGIN
SELECT title BULK COLLECT INTO titleArray
    FROM books
    WHERE category=categoryInput;
  return titleArray;
END getTitles;
END bookExample;
/

declare
  categoryInput books.category%type := 'COMPUTER';
  titleArray bookExample.bookTitleArray;

begin

titleArray := bookExample.getTitles(categoryInput);

for indx in titleArray.first .. titleArray.last
   loop
      dbms_output.put_line (titleArray (indx));
   end loop;  

 end;
/
