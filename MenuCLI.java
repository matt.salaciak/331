  private static String username;
  private static String password;

    public static void main(String[] args) {
       
        if(username == null || username.trim().isEmpty()) {
            username = System.console().readLine("Username: ");
        }
        if(password == null || password.trim().isEmpty()) {
            char[] pwd = System.console().readPassword("Password: ");
            password = new String(pwd);
        }
        readLoop();
    }


    private static void printMenu() {
        System.out.println("********************************");
        System.out.println("Select from the options below");
        System.out.println("1. Select a course");
        System.out.println("2. Add/Update a course");
        System.out.println("3. Exit");
        System.out.println("********************************");
    }

private static void readLoop () {
boolean looping = true;
System.out.println("Welcome " + username + " to the DB CLI!");

while(looping) {
printMenu();
String input = System.console().readLine();
    switch (input) {
        case "1":
        System.out.println("selecting course method");
        break;
        case "2":
        System.out.println("Adding or Updating a course method");
        break;
        case "3":
        System.out.println("Exiting");
        looping = false;
        break;
        default:
        System.out.println("option not found, pick another one");
        break;
}
}
}
