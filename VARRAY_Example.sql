
--PACKAGE DECLARATION
CREATE OR REPLACE PACAKGE bci_sample
IS
 TYPE emps_array IS VARRAY (30) OF VARCHAR2(6);

 PROCEDURE get_dept_empno (
  dno       IN   emp.deptno%TYPE,
  emps_dno  OUT  emps_array
  );
END bci_sample;


--PACKAGE BODY, DEFINE WHAT YOU'RE DOING!
CREATE OR REPLACE PACKAGE BODY bci_sample
IS
 PROCEDURE get_dept_empno (
  dno       IN   emp.deptno%TYPE,
  emps_dno  OUT  emps_array
  )
 IS
  BEGIN
   SELECT empno BULK COLLECT INTO emps_dno
    FROM emp
    WHERE deptno=dno;
  END get_dept_empno;
END bci_sample;


--Example of using varray in function
 create  type array_t is varray(2) of number;
 /

 CREATE OR REPLACE FUNCTION func return array_t
 IS
  v_array  array_t;
begin
  v_array  :=array_t(0,0);   
  v_array(1):=3;
  v_array(2):=20;
  return v_array;
end;
/

--using function in block
declare
  v_func_result array_t;
begin

 v_func_result := func();

 dbms_output.put_line(v_func_result(1));
 dbms_output.put_line(v_func_result(2));
 end;
/
