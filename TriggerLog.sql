
DROP TABLE BOOK_LOG;

CREATE TABLE BOOK_LOG (user_name VARCHAR2(30),
                        activity VARCHAR2(20),
                        event_date DATE);
                        
                        
CREATE OR REPLACE TRIGGER BOOK_CHANGE_TRIGGER
  AFTER INSERT OR UPDATE OR DELETE
  ON BOOKS
DECLARE
  log_action  BOOK_LOG.activity%TYPE;
BEGIN
  IF INSERTING THEN
    log_action := 'Insert';
  ELSIF UPDATING THEN
    log_action := 'Update';
  ELSIF DELETING THEN
    log_action := 'Delete';
  ELSE
    DBMS_OUTPUT.PUT_LINE('This code is not reachable.');
  END IF;

  INSERT INTO BOOK_LOG (user_name,activity, event_date)
    VALUES (USER, log_action,SYSDATE);
END;
                        
INSERT INTO BOOKS 
VALUES ('111111121','Do Triggers Work?','21-JAN-05',4,18.75,30.95, NULL, 'HELP');      



