 -- code to create our view
 
 
 CREATE OR REPLACE FORCE EDITIONABLE VIEW "MSALACIAK"."BOOK_SALES" ("TITLE", "ISBN", "COST", "ORDER#", "CUSTOMER#", "FIRSTNAME", "LASTNAME", "SHIPCOST") AS 
  SELECT 
        title,
        isbn,
        cost,
        order#,
        customer#,
        firstname,
        lastname,
        shipcost
    FROM
        BOOKS
    JOIN ORDERITEMS USING (ISBN)
    JOIN ORDERS USING (ORDER#)
    JOIN CUSTOMERS USING (CUSTOMER#);

-- code to create our procedure that uses a view
create or replace PROCEDURE ship_cost_sales(ship_cost IN NUMBER) 
IS
CURSOR SALES IS SELECT * FROM BOOK_SALES
WHERE shipcost > ship_cost;
BEGIN
FOR item in SALES
LOOP
dbms_output.put_line('customer name: ' || item.firstname || ' ' || item.lastname);
END LOOP;
END;


-- anonymous block to test procedure
BEGIN
ship_cost_sales(5);
END;
