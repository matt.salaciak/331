-- PACKAGE SPEC

CREATE OR REPLACE PACKAGE book_order AS
    PROCEDURE find_retail(isbn_book books.isbn%TYPE); 
END book_order;
/


-- PACKAGE BODY

CREATE OR REPLACE PACKAGE BODY book_order AS
    
    PROCEDURE find_retail(isbn_book books.isbn%TYPE) IS
    retail_price books.retail%TYPE;
    BEGIN
        SELECT retail INTO retail_price
        FROM books
        WHERE isbn = isbn_book;
         DBMS_OUTPUT.put_line('Book retail price '|| retail_price);
    END find_retail;
END book_order;
/

-- PL/SQL ANON BLOCK!
DECLARE
    isbn_code books.isbn%type := &isbn_book;
BEGIN
    book_order.find_retail(isbn_code);
END;
/

