DROP TYPE author_typ;
DROP PROCEDURE add_author;
DROP FUNCTION get_author;

create type author_typ as object(
    author_id varchar2(4),
    lname varchar2(10),
    fname varchar2(10)
);
/
CREATE OR REPLACE PROCEDURE add_author (
    vauthor IN author_typ
) IS
BEGIN
    INSERT INTO author VALUES (
        vauthor.author_id,
        vauthor.lname,
        vauthor.fname
    );
END;
/
create or replace function get_author(id varchar2) return author_typ as
vauthor author_typ;
vlname author.lname%type;
vfname author.fname%type;
begin
    select lname, fname into vlname, vfname from author where authorid =id;
    vauthor := author_typ(id, vlname, vfname);
    return vauthor;
end;
/


DECLARE
    vauthor  author_typ;
BEGIN
    vauthor := author_typ('1', 'salaciak', 'matthew');
    add_author(vauthor);
END;

