package ca.dawsoncollege;
import java.sql.SQLData;
import java.sql.SQLException;
import java.sql.SQLInput;
import java.sql.SQLOutput;

public class Author implements SQLData {
    public static final String TYPENAME = "AUTHOR_TYP";

    private String author_id;
    private String lname;
    private String fname;


    public Author(){
        
    }

    public Author(String author_id, String lname, 
    String fname){
        this.author_id = author_id;
        this.lname = lname;
        this.fname = fname;
    }

    public int getAuthorID() {
        return author_id;
    }

    public void setAuthorID(String author_id) {
        this.author_id = author_id;
    }

    public String getLastName() {
        return lname;
    }

    public void setLastName(String lname) {
        this.lname = lname;
    }

    public String getFirstName() {
        return fname;
    }

    public void setFirstName(String fname) {
        this.fname = fname;
    }
    

    @Override
    public String getSQLTypeName() throws SQLException {
        
        return Author.TYPENAME;
    }

    @Override
    //ORDER MATTERS FOR THESE!
    public void readSQL(SQLInput stream, String typeName) throws SQLException {
        setAuthorID(stream.readString());
        setLastName(stream.readString());
        setFirstName(stream.readString());
    }

    @Override
    public void writeSQL(SQLOutput stream) throws SQLException {

        stream.writeString(getAuthorID());
        stream.writeString(getLastName());
        stream.writeString(getFirstName());
    }

    @Override
    public String toString() {
        return "AuthorID=" + author_id + ", first name=" + fname + ", last name=" + lname;
    }
}

