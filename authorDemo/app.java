package ca.dawsoncollege;
import java.sql.*;
import java.util.Map;

public class App 
{
    public static void main( String[] args ) throws SQLException, ClassNotFoundException
    {
        String user = System.console().readLine("Username: ");
        String password = new String(System.console().readPassword("Password: "));
        String url = "jdbc:oracle:thin:@198.168.52.211:1521/pdbora19c.dawsoncollege.qc.ca";
        try(Connection conn = DriverManager.getConnection(url, 
        user, password)){
            Map map = conn.getTypeMap();
            map.put(Author.TYPENAME, 
            // this is the package and the class name!
            Class.forName("ca.dawsoncollege.Author"));
            conn.setTypeMap(map);

            String addProc = "{ call add_author(?)}";
            try(CallableStatement stmt = conn.prepareCall(addProc)){
                Author author = new Author("2", "kraft", "werk");
                stmt.setObject(1, author);
                stmt.execute();
            }

            String getProc = "{? = call get_author(?)}";
            try(CallableStatement stmt = conn.prepareCall(getProc)){
                stmt.registerOutParameter(1, Types.STRUCT, 
                Author.TYPENAME);
                //lets get the author we created in authorDemo.sql (its me! i'm the author!)
                stmt.setString(2, "1");
                stmt.execute();
                Author newAuthor = (Author)stmt.getObject(1);
                System.out.println(newAuthor.toString());
            }
        }
    }
}
